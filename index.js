const express = require('express');
const app = express();

const concexionDb = require('./src/config/db');
const { getTrabajadores } = require('./src/controllers/trabajadoresController');
const { getTransportes } = require('./src/controllers/transportesController');
const { getViajes, createViajes } = require('./src/controllers/viajesController');
const { getUsuarios } = require('./src/controllers/usuariosController');

concexionDb()

const PORT = 3001;
app.use(express.json());
app.use((_, res, next) => {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});

//Routes
app.route('/trabajadores')
  .get(getTrabajadores);

app.route('/transportes')
  .get(getTransportes);

app.route('/viajes')
  .get(getViajes)
  .post(createViajes)

app.route('/usuarios')
  .get(getUsuarios);



app.listen(PORT || 4000, () => {
  console.log("App started")
});