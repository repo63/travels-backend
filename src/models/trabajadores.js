const mongoose = require('mongoose');

const TrabajadoresShema = mongoose.Schema({
    nombre: {
        type: String,
        required: true,
        trim: true
    },
    apellido: {
        type: String,
        required: true,
        trim: true
    },
    rut: {
        type: String,
        required: true,
        trim: true,
        unique: true
    }
})

module.exports = mongoose.model('Trabajador', TrabajadoresShema);