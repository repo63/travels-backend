const mongoose = require('mongoose');

const ViajesShema = mongoose.Schema({
    puntoInicio: {
        type: String,
        required: true,
        trim: true
    },
    puntoTermino: {
        type: String,
        required: true,
        trim: true
    },
    km: {
        type: Number,
        required: true,
    },
    transporte: {
        type: String,
        required: true
    },
    emision: {
        type: Number,
        required: true,
    },
    personas: [
        {
            type: mongoose.Schema.Types.ObjectId,
            reft: 'Trabajador'
        }
    ],
    idaVuelta: {
        type: Boolean,
        required: true,
    },
    usuarioEmail: {
        type: String,
        required: true,
    },
    creado: {
        type: Date,
        default: Date.now()
    }
})

module.exports = mongoose.model('Viajes', ViajesShema);
