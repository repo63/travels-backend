const mongoose = require('mongoose');

const TransporteShema = mongoose.Schema({
    _id: mongoose.Schema.Types.ObjectId,
    nombre: {
        type: String,
        required: true,
        trim: true
    },
    emision: {
        type: Number,
        required: true
    }
})

module.exports = mongoose.model('Transporte', TransporteShema);