const Trabajador = require('../models/trabajadores')

exports.getTrabajadores = async (req, res) => {
    try {
        const trabajadores = await Trabajador.find({})
        res.json({ data: trabajadores })
    } catch (err) {
        res.json({ error: err })
    }
};
