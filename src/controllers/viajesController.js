const Viajes = require('../models/viajes')

exports.getViajes = async (req, res) => {
    try {
        const viajes = await Viajes.find({})
        res.json({ data: viajes })
    } catch (err) {
        res.json({ error: err })
    }
};

exports.createViajes = async (req, res) => {
    try {
        const viajes = await Viajes.create(req.body)
        res.json({ viajes })
    } catch (err) {
        res.json({ error: err })
    }
};