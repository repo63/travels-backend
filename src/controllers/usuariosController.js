const Usuario = require('../models/usuario')

exports.getUsuarios = async (req, res) => {
    try {
        const usuarios = await Usuario.find({})
        res.json({ data: usuarios })
    } catch (err) {
        res.json({ error: err })
    }
};
