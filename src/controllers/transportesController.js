const Transporte = require('../models/transporte')

exports.getTransportes = async (req, res) => {
    try {
        const transportes = await Transporte.find({})
        res.json({ data: transportes })
    } catch (err) {
        res.json({ error: err })
    }
};
