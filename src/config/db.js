const mongoose = require('mongoose');
require('dotenv').config({path: '.env'});

const concexionDb = async () => {
    try {
        await mongoose.connect(process.env.DB_MONGO, {
            useNewUrlParser: true,
            useUnifiedTopology: true,
        });
        console.log('Connected to')
    } catch (err) {
        console.log(err);
        process.exit(1);
    }
}

module.exports = concexionDb;